#include <catch2/catch_all.hpp>


#include "ini.h"

#include <sstream>
#include <iostream>

TEST_CASE("libIni::Public::TestLoadIni", "") {
  istringstream input(
R"([johan]
apples=1000
books=4300
coffee=110
tea=220

[jill]
apples=3250
books=10000
coffee=0
tea=8300
flowers=10000
)"
  );

  const Ini::Document doc = Ini::Load(input);

  REQUIRE(doc.SectionCount() == 2u);

  const Ini::Section expected_johan = {
    {"apples", "1000"},
    {"books", "4300"},
    {"coffee", "110"},
    {"tea", "220"},
  };

  const Ini::Section expected_jill = {
    {"apples", "3250"},
    {"books", "10000"},
    {"coffee", "0"},
    {"tea", "8300"},
    {"flowers", "10000"},
  };

  REQUIRE(doc.GetSection("johan") == expected_johan);
  REQUIRE(doc.GetSection("jill") == expected_jill);
}

TEST_CASE("libIni::Public::TestDocument", "") {
  Ini::Document doc;
  REQUIRE(doc.SectionCount() == 0u);

  Ini::Section* section = &doc.AddSection("one");
  REQUIRE(doc.SectionCount() == 1u);

  section->insert({"name1", "value1"});
  section->insert({"name2", "value2"});

  section = &doc.AddSection("two");
  section->insert({"name1", "value1"});
  section->insert({"name2", "value2"});
  section->insert({"name3", "value3"});

  section = &doc.AddSection("three");
  section->insert({"name1", "value1"});

  REQUIRE(doc.SectionCount() == 3u);
  const Ini::Section expected1 = {{"name1", "value1"}, {"name2", "value2"}};
  const Ini::Section expected2 = {
    {"name1", "value1"}, {"name2", "value2"}, {"name3", "value3"}
  };
  const Ini::Section expected3 = {{"name1", "value1"}};

  REQUIRE(doc.GetSection("one") == expected1);
  REQUIRE(doc.GetSection("two") == expected2);
  REQUIRE(doc.GetSection("three") == expected3);
}

TEST_CASE("libIni::Public::TestUnknownSection", "") {
  Ini::Document doc;
  doc.AddSection("primary");

  try {
    doc.GetSection("secondary");
    REQUIRE(false);
  } catch (out_of_range&) {
    REQUIRE(true);
  } catch (...) {
    REQUIRE(false);
  }
}

TEST_CASE("libIni::Public::TestDuplicateSections", "") {
  Ini::Document doc;
  doc.AddSection("one").insert({"key_1", "value1"});
  doc.AddSection("one").insert({"key_2", "value2"});

  const Ini::Section expected = {{"key_1", "value1"}, {"key_2", "value2"}};
  REQUIRE(doc.GetSection("one") == expected);
}
