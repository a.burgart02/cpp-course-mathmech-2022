#include <catch2/catch_all.hpp>

#include "badMacros.h"
#include <sstream>



TEST_CASE("PRINT_VALUES::Public::PrintsSimpleValues", "") {
    std::ostringstream output;
    PRINT_VALUES(output, 5, "red belt");
    REQUIRE(output.str() == "5\nred belt\n");
}

TEST_CASE("PRINT_VALUES::Public::PrintsExpressions", "") {
    {
        std::ostringstream output;
        PRINT_VALUES(output, 5 == 5, 1 > 0);
        REQUIRE(output.str() == "1\n1\n");
    }
    {
        std::ostringstream output;
        PRINT_VALUES(output, 3 + 2, 1 * 2);
        REQUIRE(output.str() == "5\n2\n");
    }
}

TEST_CASE("PRINT_VALUES::Public::PrintsShifts", "") {
    std::ostringstream output;
    PRINT_VALUES(output, 2 << 1, 8 >> 1);
    REQUIRE(output.str() == "4\n4\n");
}

TEST_CASE("PRINT_VALUES::Public::WorksInIfElse", "") {
    {
        std::ostringstream output;
        if (2 * 2 == 4)
            PRINT_VALUES(output, 1, 2);
        else
            PRINT_VALUES(output, 3, 4);
        REQUIRE(output.str() == "1\n2\n");
    }
    {
        std::ostringstream output;
        if (2 * 2 == 3)
            PRINT_VALUES(output, 1, 2);
        else
            PRINT_VALUES(output, 3, 4);
        REQUIRE(output.str() == "3\n4\n");
    }
}

TEST_CASE("PRINT_VALUES::Public::WorksInWhileFor", "") {
    {
        std::ostringstream output;
        int counter = 1;
        while(counter-- > 0)
            PRINT_VALUES(output, 1, 2);
        REQUIRE(output.str() == "1\n2\n");
    }
    {
        std::ostringstream output;
        for (int i = 0; i < 3; i++)
            PRINT_VALUES(output, 1, 2);
        REQUIRE(output.str() == "1\n2\n1\n2\n1\n2\n");
    }
}
