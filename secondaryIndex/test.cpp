#include <catch2/catch_all.hpp>

#include "secondaryIndex.h"



TEST_CASE("secondaryIndex::Public::TestRangeBoundaries", "")
{
    const int goodKarma = 42;
    const int badKarma = -42;

    Database db;
    db.Put({"id1", "title", "user", 1, goodKarma});
    db.Put({"id2", "title2", "user2", 2, badKarma});

    int count = 0;
    db.RangeByKarma(badKarma, goodKarma, [&count](const Record &)
                    {
    ++count;
    return true; });

    REQUIRE(2 == count);
}

TEST_CASE("secondaryIndex::Public::TestSameUser", "")
{
    Database db;
    db.Put({"id1", "Title1", "user1", 1536107260, 1000});
    db.Put({"id2", "Title2", "user1", 1536107260, 2000});

    int count = 0;
    db.AllByUser("user1", [&count](const Record &)
                 {
    ++count;
    return true; });

    REQUIRE(2 == count);
}

TEST_CASE("secondaryIndex::Public::TestReplacement", "")
{
    const std::string title = "title2";

    Database db;
    db.Put({"id", "title1", "user1", 0, 10});
    db.Erase("id");
    db.Put({"id", title, "user1", 1, -10});

    auto record = db.GetById("id");
    REQUIRE(record != nullptr);
    REQUIRE(title == record->title);
}

TEST_CASE("secondaryIndex::Public::TestPut", "")
{
    {
        const std::string title2 = "title2";
        Database db;
        bool success1 = db.Put({"id1", "title1", "user1", 0, 10});
        REQUIRE(success1);

        bool success2 = db.Put({"id2", title2, "user2", 0, 10});
        REQUIRE(success2);

        bool success3 = db.Put({"id2", "title3", "user3", 0, 10});
        REQUIRE(!success3);
        auto record = db.GetById("id2");
        REQUIRE(title2 == record->title);
    }
}

TEST_CASE("secondaryIndex::Public::TestGetById", "")
{
    {
        Database db;
        db.Put({"id1", "title1", "user1", 0, 10});
        auto record1 = db.GetById("id1");
        REQUIRE(nullptr != record1);
        auto record2 = db.GetById("id2");
        REQUIRE(nullptr == record2);
    }
}

TEST_CASE("secondaryIndex::Public::TestErase", "")
{
    {
        Database db;
        db.Put({"id1", "title1", "user1", 0, 10});
        db.Put({"id2", "title2", "user2", 0, 10});
        auto record1 = db.GetById("id1");
        REQUIRE(nullptr != record1);
        auto record2 = db.GetById("id2");
        REQUIRE(nullptr != record2);

        bool success1 = db.Erase("id1");
        REQUIRE(success1);
        auto record3 = db.GetById("id1");
        REQUIRE(nullptr == record3);

        bool success2 = db.Erase("id1");
        REQUIRE(!success2);
        auto record4 = db.GetById("id2");
        REQUIRE(nullptr != record4);
    }
}

TEST_CASE("secondaryIndex::Public::TestRangeByTimestamp", "")
{
    Database db;
    db.Put({"id1", "title1", "user1", 0, 0});
    db.Put({"id2", "title2", "user2", 2, 2});
    db.Put({"id3", "title3", "user3", 4, 4});
    {
        int count = 0;
        db.RangeByTimestamp(0, 4,
                            [&count](const Record &)
                            {
                                ++count;
                                return true;
                            });
        REQUIRE(count == 3);
    }
    {
        int count = 0;
        db.RangeByTimestamp(1, 3,
                            [&count](const Record &)
                            {
                                ++count;
                                return true;
                            });
        REQUIRE(count == 1);
    }
    {
        int count = 0;
        db.RangeByTimestamp(2, 2,
                            [&count](const Record &)
                            {
                                ++count;
                                return true;
                            });
        REQUIRE(count == 1);
    }
    {
        int count = 0;
        db.RangeByTimestamp(100, 1000,
                            [&count](const Record &)
                            {
                                ++count;
                                return true;
                            });
        REQUIRE(count == 0);
    }
}

TEST_CASE("secondaryIndex::Public::TestRangeByKarma", "")
{
    Database db;
    db.Put({"id1", "title1", "user1", 0, 0});
    db.Put({"id2", "title2", "user2", 2, 2});
    db.Put({"id3", "title3", "user3", 4, 4});
    {
        int count = 0;
        db.RangeByKarma(0, 4,
                        [&count](const Record &)
                        {
                            ++count;
                            return true;
                        });
        REQUIRE(count == 3);
    }
    {
        int count = 0;
        db.RangeByKarma(1, 3,
                        [&count](const Record &)
                        {
                            ++count;
                            return true;
                        });
        REQUIRE(count == 1);
    }
    {
        int count = 0;
        db.RangeByKarma(2, 2,
                        [&count](const Record &)
                        {
                            ++count;
                            return true;
                        });
        REQUIRE(count == 1);
    }
    {
        int count = 0;
        db.RangeByKarma(100, 1000,
                        [&count](const Record &)
                        {
                            ++count;
                            return true;
                        });
        REQUIRE(count == 0);
    }
}

TEST_CASE("secondaryIndex::Public::TestRangeStop", "")
{
    Database db;
    db.Put({"id1", "title1", "user1", 0, 0});
    db.Put({"id2", "title2", "user1", 2, 2});
    db.Put({"id3", "title3", "user1", 4, 4});
    {
        int count = 0;
        db.RangeByTimestamp(-100, 100,
                            [&count](const Record &)
                            {
                                ++count;
                                return false;
                            });
        REQUIRE(count == 1);
    }
    {
        int count = 0;
        db.RangeByKarma(-100, 100,
                        [&count](const Record &)
                        {
                            ++count;
                            return false;
                        });
        REQUIRE(count == 1);
    }
    {
        int count = 0;
        db.AllByUser("user1",
                     [&count](const Record &)
                     {
                         ++count;
                         return false;
                     });
        REQUIRE(count == 1);
    }
}
