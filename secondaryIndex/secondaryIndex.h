#pragma once

#include <iostream>
#include <map>
#include <string>
#include <unordered_map>

struct Record {
  std::string id;
  std::string title;
  std::string user;
  int timestamp;
  int karma;
};

class Database {
public:
  bool Put(const Record& record);
  const Record* GetById(const std::string& id) const;
  bool Erase(const std::string& id);

  template <typename Callback>
  void RangeByTimestamp(int low, int high, Callback callback) const;

  template <typename Callback>
  void RangeByKarma(int low, int high, Callback callback) const;

  template <typename Callback>
  void AllByUser(const std::string& user, Callback callback) const;
};
