#include <catch2/catch_all.hpp>

#include <forward_list>
#include <iterator>
#include <vector>
#include <algorithm>
#include <iostream>
#include <iostream>

#include "hashSet.h"

struct IntHasher
{
    size_t operator()(int value) const
    {
        return value;
    }
};

struct TestValue
{
    int value;

    bool operator==(TestValue other) const
    {
        return value / 2 == other.value / 2;
    }
};

struct TestValueHasher
{
    size_t operator()(TestValue value) const
    {
        return value.value / 2;
    }
};

TEST_CASE("hashSet::Public::SmokeTest", "")
{
    HashSet<int, IntHasher> hset(2);
    hset.Add(3);
    hset.Add(4);

    REQUIRE(hset.Has(3));
    REQUIRE(hset.Has(4));
    REQUIRE(!hset.Has(5));

    hset.Erase(3);

    REQUIRE(!hset.Has(3));
    REQUIRE(hset.Has(4));
    REQUIRE(!hset.Has(5));

    hset.Add(3);
    hset.Add(5);

    REQUIRE(hset.Has(3));
    REQUIRE(hset.Has(4));
    REQUIRE(hset.Has(5));
}

TEST_CASE("hashSet::Public::TestEmpty", "")
{
    HashSet<int, IntHasher> hset(10);
    for (int value = 0; value < 10001; value++)
    {
        REQUIRE(!hset.Has(value));
    }
}

TEST_CASE("hashSet::Public::TestIdempotency", "")
{
    HashSet<int, IntHasher> hset(10);
    hset.Add(42);
    REQUIRE(hset.Has(42));
    hset.Add(42);
    REQUIRE(hset.Has(42));
    hset.Erase(42);
    REQUIRE(!hset.Has(42));
    hset.Erase(42);
    REQUIRE(!hset.Has(42));
}

TEST_CASE("hashSet::Public::TestEquivalence", "")
{
    HashSet<TestValue, TestValueHasher> hset(10);
    hset.Add(TestValue{2});
    hset.Add(TestValue{3});

    REQUIRE(hset.Has(TestValue{2}));
    REQUIRE(hset.Has(TestValue{3}));

    const auto &bucket = hset.GetBucket(TestValue{2});
    const auto &three_bucket = hset.GetBucket(TestValue{3});
    REQUIRE(&bucket == &three_bucket);

    REQUIRE(1 == distance(begin(bucket), end(bucket)));
    REQUIRE(2 == bucket.front().value);
}
