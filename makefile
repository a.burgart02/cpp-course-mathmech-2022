VENV ?= .venv

not-mac-init:
	sudo apt install python3.10 python3.10-venv python3.10-dev python3.10-distutils gcc
	git submodule update --init --recursive

mac-init:
	brew install python@3.10 gcc
	git submodule update --init --recursive

init:
	python3.10 -m venv $(VENV)
	$(VENV)/bin/python -m pip install --upgrade pip
	$(VENV)/bin/python -m pip install poetry
	$(VENV)/bin/poetry install

lint:
	$(VENV)/bin/black --skip-string-normalization --check $(CODE)
	$(VENV)/bin/flake8 --show-source $(CODE)
	$(VENV)/bin/mypy $(CODE)

pretty:
	$(VENV)/bin/isort $(CODE)
	$(VENV)/bin/black --skip-string-normalization $(CODE)

plint: pretty lint

lock: poetry lock
