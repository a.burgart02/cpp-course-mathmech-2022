#!/usr/bin/env python3
import argparse
import configparser
import functools
import glob
import json
import logging
import os
import re
import shutil
import stat
import subprocess

CONFIG_PATH = ".tester.json"
SUBMIT_REPO = ".submit-repo"
MIRROR_REMOTE = "local"
FORK_REMOTE = "student"
OUT_OF_DATE_REGEX = re.compile(r"main.*?(local out of date)")


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


def execute_command(cmd, from_dir, *args):
    cmd_line = " ".join([cmd] + list(args))
    res = subprocess.check_output(cmd_line, cwd=from_dir, stderr=subprocess.PIPE, shell=True)
    return res.strip().decode("utf-8")


def edit_config(filename, update_dict):
    config = configparser.ConfigParser()
    config.read(filename)
    for section, vals in update_dict.items():
        section_config = {}
        if section in config.sections():
            section_config = config[section]
        section_config.update(vals)
        config[section] = section_config

    with open(filename, "w") as configfile:
        config.write(configfile)


def try_change_access_rights(func, path, exc_info):
    if not os.access(path, os.W_OK):
        os.chmod(path, stat.S_IWUSR)
        func(path)
    else:
        raise


def remove_recursive_if_exist(path):
    if os.path.isdir(path):
        shutil.rmtree(path, onerror=try_change_access_rights)


class TaskSubmitService:
    def __init__(
        self, submit_repo_location="../.submit-repo", mirror_location="..", mirror_remote="local", fork_remote="student"
    ):
        self._submit_location = submit_repo_location
        self._mirror_location = mirror_location
        self._mirror_remote = mirror_remote
        self._grader_ci = os.path.join(mirror_location, ".grader-ci.yml")
        self._fork_remote = fork_remote
        self._git = functools.partial(execute_command, "git")
        self._set_up_auth()
        logger.debug("TaskSubmitService: initialized")

    def _set_up_auth(self):
        self._remote_url = self._git(self._mirror_location, "remote", "get-url", self._fork_remote)
        self._name = self._git(self._mirror_location, "config", "user.name")
        self._email = self._git(self._mirror_location, "config", "user.email")

    def reset(self):
        logger.debug(f"TaskSubmitService: reset {self._submit_location}")
        self._reset_dirs()
        self._set_up_repo()

    def check_if_repos_is_up_to_date(self):
        fork_output = self._git(self._submit_location, "remote", "show", "upstream")
        repo_output = self._git(".", "remote", "show", "origin")
        fork_up_to_date = OUT_OF_DATE_REGEX.search(fork_output) is None
        repo_up_to_date = OUT_OF_DATE_REGEX.search(repo_output) is None

        if not fork_up_to_date:
            logger.warning("Ветка main репозитория форка неактульна.")
        if not repo_up_to_date:
            logger.warning("Ветка main публичного репозитория неактульна.")
        return fork_up_to_date and repo_up_to_date

    def _reset_dirs(self):
        remove_recursive_if_exist(self._submit_location)
        os.makedirs(self._submit_location)

    def _set_up_repo(self):
        self._git(self._submit_location, "init")
        self._git(self._submit_location, "remote", "add", self._mirror_remote, self._mirror_location)

        remote_url = self._git(self._mirror_location, "remote", "get-url", self._fork_remote)
        self._git(self._submit_location, "remote", "add", self._fork_remote, remote_url)
        origin_url = self._git(self._mirror_location, "remote", "get-url", "origin")
        self._git(self._submit_location, "remote", "add", "upstream", origin_url)

        update_dict = {"user": {"name": self._name, "email": self._email}}
        submit_repo_git_config = os.path.join(self._submit_location, ".git", "config")
        edit_config(submit_repo_git_config, update_dict)

        self._git(self._submit_location, "fetch", "student")
        self._git(self._submit_location, "checkout", "--track", "student/main")

    def _checkout_branch(self, branch_name):
        try:
            self._git(self._submit_location, 'branch', '-d', branch_name)
        except Exception:
            pass
        finally:
            self._git(self._submit_location, "checkout", "-b", branch_name)

    def _copy_files(self, dir_from, dir_to, filenames):
        for filename in filenames:
            directory = os.path.join(dir_to, os.path.dirname(filename))
            for path in glob.glob(f"{dir_from}/{filename}"):
                if not os.path.isdir(directory):
                    os.makedirs(directory)
                shutil.copy(path, directory)

    def copy_solution_files(self, task_name, filenames):
        logger.debug("TaskSubmitService: copy_solution_files")
        submit_repo_task_dir = os.path.join(self._submit_location, task_name)
        if not os.path.isdir(submit_repo_task_dir):
            os.makedirs(submit_repo_task_dir)
        dir_to = os.path.join(self._submit_location, task_name)
        dir_from = os.path.join(self._mirror_location, task_name)
        self._copy_files(dir_from, dir_to, filenames)

    def _commit_solution_files(self, task_name, filenames):
        self._checkout_branch(f"submits/{task_name}")

        shutil.copyfile(self._grader_ci, os.path.join(self._submit_location, ".gitlab-ci.yml"))
        self._git(self._submit_location, "add", ".gitlab-ci.yml")

        self.copy_solution_files(task_name, filenames)

        self._git(self._submit_location, "add", task_name)
        self._git(self._submit_location, "commit", "-m", task_name, "--allow-empty")

    def commit_solution(self, task_name, filenames):
        logger.debug("TaskSubmitService: commit_solution")
        self._commit_solution_files(task_name, filenames)

    def _try_push(self, remote, branch, force=False):
        flags = "-f" if force else ""
        cmd = f"push {flags} {remote} {branch}"
        try:
            self._git(self._submit_location, cmd)
            return True
        except Exception:
            return False

    def push_solution(self, task_name):
        logger.debug("TaskSubmitService: push_solution")
        self._try_push(self._mirror_remote, f"submits/{task_name}", False)
        self._try_push(self._fork_remote, "initial", True)
        self._try_push(self._fork_remote, f"submits/{task_name}", True)

    def get_task_branch(self, task_name):
        remote_url = self._git(self._mirror_location, "remote", "get-url", self._fork_remote)
        remote_url = self._make_http_url(remote_url)

        return f'{remote_url}/-/tree/submits/{task_name}'

    def _make_http_url(self, url):
        if url.startswith('git'):  # is ssh -> make http
            url = f'https://gitlab.com/{url.split(":")[1]}'

        if url.endswith(".git"):
            return url[:-4]  # remove .git

        return url


def parse_args():
    parser = argparse.ArgumentParser(
        "Submit CLI to submit solution for review", description="Go to Task folder and run 'python ../submit.py'"
    )
    parser.add_argument("-v", "--verbose", help="show debug messages", action="store_true")

    parser.add_argument("-t", "--task", metavar="TASK", type=str, help="Task name", default=None)

    return parser.parse_args()


def main():
    args = parse_args()

    verbose = args.verbose
    if not verbose:
        logger.setLevel(logging.INFO)
    task_name = args.task

    mirror_repo_location = "."

    if not task_name:
        task_name = os.path.basename(os.path.realpath("."))
        mirror_repo_location = os.pardir

    submit_repo_location = os.path.join(mirror_repo_location, SUBMIT_REPO)

    config_path = os.path.join(mirror_repo_location, task_name, CONFIG_PATH)
    if not os.path.isfile(config_path):
        logger.critical(f"Task config {config_path} not found")
        logger.critical("Are you running from correct directory?")
        exit(1)

    logger.debug(f"reading config file: {config_path}")
    with open(config_path, encoding="utf-8", mode="r") as rf:
        task_config = json.loads(rf.read())

    files_to_submit = task_config["allow_change"] + [".tester.json"]

    task_submit_service = TaskSubmitService(submit_repo_location, mirror_repo_location, MIRROR_REMOTE, FORK_REMOTE)

    task_submit_service.reset()
    all_up_to_date = task_submit_service.check_if_repos_is_up_to_date()
    if not all_up_to_date:
        choice = input("Публичный репозиторий или форк содержат неактульные изменения, продолжить? (yes/no): ")
        if choice != "yes":
            logger.info("Отмена отправки решения...")
            os._exit(0)
            return
        logger.info("Продолжение отправки решения")
    task_submit_service.commit_solution(task_name, files_to_submit)
    task_submit_service.push_solution(task_name)
    task_branch_url = task_submit_service.get_task_branch(task_name)

    logger.info('Результаты проверки можно увидеть перейдя по ссылке:')
    logger.info('%s', task_branch_url)


if __name__ == "__main__":
    main()
