#pragma once

#include "grammar_transform.h"


enum PDA_ParserAction {
    PDA_ParserAction_None,
    PDA_ParserAction_Token,
    PDA_ParserAction_Rule,
};

class PDA_Parser {
public:
    PDA_Parser(const LSyntax::Grammar& grammar, const PARSING_TABLE& parsing_table);
    void ParseDry(const std::vector<LSyntax::Token>& input);
    void SetProgram(const std::vector<LSyntax::Token>& input);
    void ResetStack();
    bool MoveNext();
    LSyntax::Token CurrentToken();
    LSyntax::Rule CurrentRule();
    PDA_ParserAction LastParserAction();
    bool IsValid();
private:
    LSyntax::Grammar grammar;
    PARSING_TABLE parsing_table;
    std::vector<LSyntax::Token> input;
    std::vector<LSyntax::Token> stack;
    size_t instruction_pointer = 0;

    PDA_ParserAction lastParserAction = PDA_ParserAction::PDA_ParserAction_None;
    LSyntax::Token currentToken;
    LSyntax::Rule currentRule;
};