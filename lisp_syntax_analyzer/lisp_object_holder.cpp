#include "lisp_object.h"
#include "lisp_object_holder.h"
#include <iostream>


namespace LRuntime {


ObjectHolder ObjectHolder::Share(Object& object) {
  return ObjectHolder(std::shared_ptr<Object>(&object, [](auto*) { /* do nothing */ }));
}

ObjectHolder ObjectHolder::None() {
  return ObjectHolder();
}

Object& ObjectHolder::operator *() {
  return *Get();
}

const Object& ObjectHolder::operator *() const {
  return *Get();
}

Object* ObjectHolder::operator ->() {
  return Get();
}

const Object* ObjectHolder::operator ->() const {
  return Get();
}

Object* ObjectHolder::Get() {
  return data.get();
}

const Object* ObjectHolder::Get() const {
  return data.get();
}

ObjectHolder::operator bool() const {
  return Get();
}

bool IsTrue(ObjectHolder object) {
  // TODO: Implement

  return false;
}

bool EqualComparator(ObjectHolder lhs, ObjectHolder rhs) {
  // TODO: Implement
    return lhs.Get() == rhs.Get();
}

bool LessComparator(ObjectHolder lhs, ObjectHolder rhs) {
  // TODO: Implement
    return lhs.Get() < rhs.Get();
}

    
}