#include <catch2/catch_all.hpp>


#include <sstream>
#include <vector>
#include <iostream>
#include <map>

#include "lisp_lexer.h"

#include "trie.h"
#include "token.h"
#include "grammar_transform.h"
#include "lisp_parser.h"



TEST_CASE("Public: TestPrepareGrammar", "") {
    LSyntax::Token E(LSyntax::TokenType::NON_TERMINAL, "E");
    LSyntax::Token T(LSyntax::TokenType::NON_TERMINAL, "T");
    LSyntax::Token F(LSyntax::TokenType::NON_TERMINAL, "F");
    LSyntax::Token plus(LSyntax::TokenType::TERMINAL, "+");
    LSyntax::Token minus(LSyntax::TokenType::TERMINAL, "-");
    LSyntax::Token mult(LSyntax::TokenType::TERMINAL, "*");
    LSyntax::Token divide(LSyntax::TokenType::TERMINAL, "/");
    LSyntax::Token id(LSyntax::TokenType::TERMINAL, "id");
    LSyntax::Token lpar(LSyntax::TokenType::TERMINAL, "(");
    LSyntax::Token rpar(LSyntax::TokenType::TERMINAL, ")");

    std::map<LSyntax::Token, std::set<LSyntax::Rule>> grammarParts = {
        {E, { LSyntax::Rule(E, {E, plus, T}), LSyntax::Rule(E, {E, minus, T}), LSyntax::Rule(E, {T})}},
        {T, { LSyntax::Rule(T, {T, mult, F}), LSyntax::Rule(T, {T, divide, F}), LSyntax::Rule(T, {F})}},
        {F, { LSyntax::Rule(F, {lpar, E, rpar}), LSyntax::Rule(F, {id})}},
    };
    LSyntax::Grammar grammar(grammarParts);
    grammar.axiom = E;

    FIRST first;
    FOLLOW follow;
    SELECT select;
    PARSING_TABLE parse_table;
    PrepareGrammar(grammar, first, follow, select, parse_table);

    bool res = IsLL1(grammar, select);

    REQUIRE(res);
}

TEST_CASE("Public: TestReadProgram", "") {
    std::vector<LLexer::LexToken> ltokens;
    std::vector<LSyntax::Token> stokens;
    std::istringstream iss("(print \"123\")");
    ReadProgram(iss, ltokens, stokens);

    std::vector<LLexer::LexToken> expectedLTokens = {
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_PRINT, "print"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_LITERAL, "123"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_END, ""),
    };

    std::vector<LSyntax::Token> expectedSTokens = {
        LSyntax::Token(LSyntax::TokenType::TERMINAL, "("),
        LSyntax::Token(LSyntax::TokenType::TERMINAL, "PRINT"),
        LSyntax::Token(LSyntax::TokenType::TERMINAL, "STRING"),
        LSyntax::Token(LSyntax::TokenType::TERMINAL, ")"),
        LSyntax::EndOfStr,
    };

    REQUIRE(ltokens == expectedLTokens);
    REQUIRE(stokens == expectedSTokens);
}

TEST_CASE("Public: TestConvertStackFrameToStatement", "") {
    StackFrame sf;
    sf.type = StackFrameType::StackFrameType_DEFUN;
    sf.name = "sum";
    sf.names = {"arg1", "arg2"};
    sf.statements.push_back(std::make_unique<LAst::Add>(
        std::make_unique<LAst::VarValue>("arg1"),
        std::make_unique<LAst::VarValue>("arg2")
    ));
    std::unique_ptr<LAst::LStatement> stmt = ConvertStackFrameToStatement(std::move(sf));
    LRuntime::Closure closure = {
        {"a", ObjectHolder::Own(LRuntime::Number(2))},
        {"b", ObjectHolder::Own(LRuntime::Number(2))}
    };
    ObjectHolder defun_res = stmt->Execute(closure);
    std::vector<std::unique_ptr<LAst::LStatement>> args;
    args.push_back(std::make_unique<LAst::VarValue>("a"));
    args.push_back(std::make_unique<LAst::VarValue>("b"));
    std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("sum", std::move(args)));
    ObjectHolder funcall_res = func_call->Execute(closure);

    REQUIRE(funcall_res.TryAs<LRuntime::Number>() != nullptr);
    REQUIRE(funcall_res.TryAs<LRuntime::Number>()->GetValue() == 4);
}


TEST_CASE("Public: TestAddBuiltIns", "") {
    LRuntime::Closure closure;
    AddBuiltIns(closure);
    closure["a"] = ObjectHolder::Own(LRuntime::Number(5));
    closure["b"] = ObjectHolder::Own(LRuntime::Number(2));
    closure["yes"] = ObjectHolder::Own(LRuntime::Bool(true));
    closure["no"] = ObjectHolder::Own(LRuntime::Bool(false));
    closure["l1"] = ObjectHolder::Own(LRuntime::LList(
        {
            ObjectHolder::Own(LRuntime::Number(1)),
            ObjectHolder::Own(LRuntime::Number(2)),
            ObjectHolder::Own(LRuntime::Number(3)),
        }
    ));
    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::VarValue>("a"));
        args.push_back(std::make_unique<LAst::VarValue>("b"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("-", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(funcall_res.TryAs<LRuntime::Number>()->GetValue() == 3);
    }
    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::VarValue>("a"));
        args.push_back(std::make_unique<LAst::VarValue>("b"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("*", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(funcall_res.TryAs<LRuntime::Number>()->GetValue() == 10);
    }
    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::VarValue>("a"));
        args.push_back(std::make_unique<LAst::VarValue>("b"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("/", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(std::abs(funcall_res.TryAs<LRuntime::Number>()->GetValue() - 2.5) < 1e-9);
    }
    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::VarValue>("a"));
        args.push_back(std::make_unique<LAst::VarValue>("b"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("<", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(!funcall_res.TryAs<LRuntime::Bool>()->GetValue());
    }
    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::VarValue>("yes"));
        args.push_back(std::make_unique<LAst::VarValue>("no"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("and", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(!funcall_res.TryAs<LRuntime::Bool>()->GetValue());
    }
    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::VarValue>("yes"));
        args.push_back(std::make_unique<LAst::VarValue>("no"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("or", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(funcall_res.TryAs<LRuntime::Bool>()->GetValue());
    }
    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::VarValue>("yes"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("not", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(!funcall_res.TryAs<LRuntime::Bool>()->GetValue());
    }

    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::VarValue>("l1"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("car", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(funcall_res.TryAs<LRuntime::Number>()->GetValue() == 1);
    }
    {
        std::vector<std::unique_ptr<LAst::LStatement>> args;
        args.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(0)));
        args.push_back(std::make_unique<LAst::VarValue>("l1"));
        std::unique_ptr<LAst::FuncCallStatement> func_call(new LAst::FuncCallStatement("nth", std::move(args)));
        ObjectHolder funcall_res = func_call->Execute(closure);

        REQUIRE(funcall_res.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(funcall_res.TryAs<LRuntime::Number>()->GetValue() == 1);
    }
}


TEST_CASE("Public: TestIsTrue", "") {
    {
        REQUIRE(IsTrue(ObjectHolder::Own(LRuntime::Number(1))));
        REQUIRE(!IsTrue(ObjectHolder::Own(LRuntime::Number(0))));
    }
    {
        REQUIRE(IsTrue(ObjectHolder::Own(LRuntime::String("123"))));
        REQUIRE(!IsTrue(ObjectHolder::Own(LRuntime::String(""))));
    }
    {
        REQUIRE(IsTrue(ObjectHolder::Own(LRuntime::Bool(true))));
        REQUIRE(!IsTrue(ObjectHolder::Own(LRuntime::Bool(false))));
    }
}

TEST_CASE("Public: TestEqualComparator", "") {
    {
        REQUIRE(EqualComparator(
            ObjectHolder::Own(LRuntime::Number(1)),
            ObjectHolder::Own(LRuntime::Number(1))
        ));
        REQUIRE(!EqualComparator(
            ObjectHolder::Own(LRuntime::Number(1)),
            ObjectHolder::Own(LRuntime::Number(2))
        ));
    }
    {
        REQUIRE(EqualComparator(
            ObjectHolder::Own(LRuntime::String("1")),
            ObjectHolder::Own(LRuntime::String("1"))
        ));
        REQUIRE(!EqualComparator(
            ObjectHolder::Own(LRuntime::String("1")),
            ObjectHolder::Own(LRuntime::String("2"))
        ));
    }
    {
        REQUIRE(EqualComparator(
            ObjectHolder::Own(LRuntime::Bool(true)),
            ObjectHolder::Own(LRuntime::Bool(true))
        ));
        REQUIRE(!EqualComparator(
            ObjectHolder::Own(LRuntime::Bool(true)),
            ObjectHolder::Own(LRuntime::Bool(false))
        ));
    }
    
}


TEST_CASE("Public: TestLessComparator", "") {
    {
        REQUIRE(LessComparator(
            ObjectHolder::Own(LRuntime::Number(1)),
            ObjectHolder::Own(LRuntime::Number(2))
        ));
        REQUIRE(!LessComparator(
            ObjectHolder::Own(LRuntime::Number(2)),
            ObjectHolder::Own(LRuntime::Number(2))
        ));
    }
    {
        REQUIRE(LessComparator(
            ObjectHolder::Own(LRuntime::String("1")),
            ObjectHolder::Own(LRuntime::String("2"))
        ));
        REQUIRE(!LessComparator(
            ObjectHolder::Own(LRuntime::String("1")),
            ObjectHolder::Own(LRuntime::String("1"))
        ));
    }
    {
        REQUIRE(LessComparator(
            ObjectHolder::Own(LRuntime::Bool(false)),
            ObjectHolder::Own(LRuntime::Bool(true))
        ));
        REQUIRE(!LessComparator(
            ObjectHolder::Own(LRuntime::Bool(true)),
            ObjectHolder::Own(LRuntime::Bool(true))
        ));
    }
}

TEST_CASE("Public: TestPrint", "") {
    {
        std::stringstream ss;
        LRuntime::Bool objT(true);
        LRuntime::Bool objNil(false);
        objT.Print(ss);
        ss << " ";
        objNil.Print(ss);

        REQUIRE(ss.str() == "T Nil");
    }

    {
        std::stringstream ss;
        LRuntime::LFunction objFunc("name", {"a1", "a2"}, {});
        objFunc.Print(ss);

        REQUIRE(ss.str() == "(name a1 a2 )");
    }

    {
        std::stringstream ss;
        LRuntime::LList objListEmpty({});
        LRuntime::LList objList({
            ObjectHolder::Own(LRuntime::Number(1)),
            ObjectHolder::Own(LRuntime::Number(2))
        });
        objListEmpty.Print(ss);
        ss << " ";
        objList.Print(ss);

        REQUIRE(ss.str() == "Nil (1 2 )");
    }
}



TEST_CASE("Public: TestListGet", "") {
    {
        LRuntime::LList objList({
            ObjectHolder::Own(LRuntime::Number(1)),
            ObjectHolder::Own(LRuntime::Number(2))
        });

        REQUIRE(objList.Get(0).TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(objList.Get(0).TryAs<LRuntime::Number>()->GetValue() == 1);
    }
}



TEST_CASE("Public: TestLFunctionCall", "") {
    {
        LRuntime::Closure closure;

        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::VarValue>("arg"));
        LRuntime::LFunction objFunc("id", {"arg"}, std::move(body));
        ObjectHolder val = objFunc.Call({ObjectHolder::Own(LRuntime::Number(1))}, closure);

        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Number>()->GetValue() == 1);
    }
}


TEST_CASE("Public: TestStatements", "") {
    {
        LRuntime::Closure closure;
        closure["a"] = ObjectHolder::Own(LRuntime::Number(1));
        LAst::VarValue stmt("a");
        ObjectHolder val = stmt.Execute(closure);

        
        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Number>()->GetValue() == 1);
    }

    {
        LRuntime::Closure closure;
        closure["a"] = ObjectHolder::Own(LRuntime::Number(1));
        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::VarValue>("arg"));
        LAst::DefunStatement stmt("id", {"arg"}, std::move(body));
        stmt.Execute(closure);
        REQUIRE(closure.find("id") != closure.end());

        std::vector<std::unique_ptr<LAst::LStatement>> body2;
        body2.push_back(std::make_unique<LAst::VarValue>("a"));
        LAst::FuncCallStatement funcCallStmt("id", std::move(body2));
        ObjectHolder val2 = funcCallStmt.Execute(closure);
        
        REQUIRE(val2.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val2.TryAs<LRuntime::Number>()->GetValue() == 1);
    }

    {
        LRuntime::Closure closure;
        LAst::DefvarStatement stmt("a", std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        stmt.Execute(closure);
        REQUIRE(closure.find("a") != closure.end());
        REQUIRE(closure["a"].TryAs<LRuntime::Number>());
        REQUIRE(closure["a"].TryAs<LRuntime::Number>()->GetValue() == 1);
    }

    {
        LRuntime::Closure closure;
        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        LAst::ListStatement stmt(std::move(body));
        ObjectHolder val = stmt.Execute(closure);
            
        REQUIRE(val.TryAs<LRuntime::LList>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.size() == 1);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.begin()->TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.begin()->TryAs<LRuntime::Number>()->GetValue() == 1);
    }

    {
        LRuntime::Closure closure;
        LAst::IfStatement stmt(
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(false)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(1)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Number>()->GetValue() == 2);

        LAst::IfStatement stmt2(
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(true)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(1)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2))
        );
        ObjectHolder val2 = stmt2.Execute(closure);
        REQUIRE(val2.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val2.TryAs<LRuntime::Number>()->GetValue() == 1);
    }

    {
        LRuntime::Closure closure;
        LAst::Or stmt(
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(true)),
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(false))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Bool>()->GetValue());

        LAst::Or stmt2(
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(false)),
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(false))
        );
        ObjectHolder val2 = stmt2.Execute(closure);
        REQUIRE(val2.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(!val2.TryAs<LRuntime::Bool>()->GetValue());
    }

    {
        LRuntime::Closure closure;
        LAst::And stmt(
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(true)),
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(true))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Bool>()->GetValue());

        LAst::And stmt2(
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(true)),
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(false))
        );
        ObjectHolder val2 = stmt2.Execute(closure);
        REQUIRE(val2.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(!val2.TryAs<LRuntime::Bool>()->GetValue());
    }

    {
        LRuntime::Closure closure;
        LAst::Not stmt(
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(false))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Bool>()->GetValue());

        LAst::Not stmt2(
            std::make_unique<LAst::BoolConst>(LRuntime::Bool(true))
        );
        ObjectHolder val2 = stmt2.Execute(closure);
        REQUIRE(val2.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(!val2.TryAs<LRuntime::Bool>()->GetValue());
    }

    {
        LRuntime::Closure closure;
        LAst::Add stmt(
            std::make_unique<LAst::NumericConst>(LRuntime::Number(1)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Number>()->GetValue() == 3);
    }

    {
        LRuntime::Closure closure;
        LAst::Sub stmt(
            std::make_unique<LAst::NumericConst>(LRuntime::Number(5)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Number>()->GetValue() == 3);
    }

    {
        LRuntime::Closure closure;
        LAst::Mult stmt(
            std::make_unique<LAst::NumericConst>(LRuntime::Number(5)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Number>()->GetValue() == 10);
    }

    {
        LRuntime::Closure closure;
        LAst::Div stmt(
            std::make_unique<LAst::NumericConst>(LRuntime::Number(5)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(std::abs(val.TryAs<LRuntime::Number>()->GetValue() - 2.5) < 1e-9);
    }

    {
        LRuntime::Closure closure;
        LAst::Comparison stmt(
            LRuntime::LessComparator,
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(5))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Bool>()->GetValue());
        

        LAst::Comparison stmt2(
            LRuntime::LessComparator,
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(2))
        );
        ObjectHolder val2 = stmt2.Execute(closure);
        REQUIRE(val2.TryAs<LRuntime::Bool>() != nullptr);
        REQUIRE(!val2.TryAs<LRuntime::Bool>()->GetValue());
    }

    {
        LRuntime::Closure closure;
        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        LAst::Nth stmt(
            std::make_unique<LAst::NumericConst>(LRuntime::Number(0)),
            std::make_unique<LAst::ListStatement>(std::move(body))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Number>()->GetValue() == 1);
    }

    {
        LRuntime::Closure closure;
        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        LAst::Cons stmt(
            std::make_unique<LAst::NumericConst>(LRuntime::Number(0)),
            std::make_unique<LAst::ListStatement>(std::move(body))
        );
        ObjectHolder val = stmt.Execute(closure);

        REQUIRE(val.TryAs<LRuntime::LList>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.size() == 2);

        LAst::Cons stmt2(
            std::make_unique<LAst::NumericConst>(LRuntime::Number(0)),
            std::make_unique<LAst::NumericConst>(LRuntime::Number(1))
        );
        ObjectHolder val2 = stmt2.Execute(closure);
        REQUIRE(val2.TryAs<LRuntime::LList>() != nullptr);
        REQUIRE(val2.TryAs<LRuntime::LList>()->args.size() == 2);
    }

    {
        LRuntime::Closure closure;
        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        LAst::Rest stmt(
            std::make_unique<LAst::ListStatement>(std::move(body))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::LList>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.size() == 0);

        std::vector<std::unique_ptr<LAst::LStatement>> body2;
        body2.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        body2.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(2)));
        LAst::Rest stmt2(
            std::make_unique<LAst::ListStatement>(std::move(body2))
        );
        ObjectHolder val2 = stmt2.Execute(closure);
        REQUIRE(val2.TryAs<LRuntime::LList>() != nullptr);
        REQUIRE(val2.TryAs<LRuntime::LList>()->args.size() == 1);
    }

    {
        LRuntime::Closure closure;
        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(2)));
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(3)));
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        LAst::Sort stmt(
            std::make_unique<LAst::ListStatement>(std::move(body))
        );
        ObjectHolder val = stmt.Execute(closure);
            
        REQUIRE(val.TryAs<LRuntime::LList>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.size() == 3);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.begin()->TryAs<LRuntime::Number>()->GetValue() == 1);
    }

    {
        LRuntime::Closure closure;
        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(2)));
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(3)));
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        LAst::Car stmt(
            std::make_unique<LAst::ListStatement>(std::move(body))
        );
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::Number>()->GetValue() == 2);
    }

    {
        LRuntime::Closure closure;
        std::vector<std::unique_ptr<LAst::LStatement>> funcBody;
        funcBody.push_back(std::make_unique<LAst::VarValue>("arg"));
        closure["id"] = ObjectHolder::Own(LRuntime::LFunction("id", {"arg"}, std::move(funcBody)));

        std::vector<std::unique_ptr<LAst::LStatement>> body;
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(1)));
        body.push_back(std::make_unique<LAst::NumericConst>(LRuntime::Number(2)));
        LAst::MapList stmt(
            std::make_unique<LAst::StringConst>(LRuntime::String("id")),
            std::make_unique<LAst::ListStatement>(std::move(body))
        );
        ObjectHolder val = stmt.Execute(closure);
            
        REQUIRE(val.TryAs<LRuntime::LList>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.size() == 2);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.begin()->TryAs<LRuntime::Number>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::LList>()->args.begin()->TryAs<LRuntime::Number>()->GetValue() == 1);
    }

    {
        LRuntime::Closure closure;
        LAst::QuoteStatement stmt("id");
        ObjectHolder val = stmt.Execute(closure);
        REQUIRE(val.TryAs<LRuntime::String>() != nullptr);
        REQUIRE(val.TryAs<LRuntime::String>()->GetValue() == "id");
    }
}