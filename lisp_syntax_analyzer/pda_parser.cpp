
#include "pda_parser.h"

void PrintTokens(const std::vector<LSyntax::Token>& tokens, int ip = -1) {
    int tokenCount = tokens.size();
    for(int i = 0; i < tokenCount; i++) {
        if (ip == i) {
            std::cout << ">";
        }
        std::cout << tokens[i];
    }
    std::cout << std::endl;
}


PDA_Parser::PDA_Parser(const LSyntax::Grammar& grammar, const PARSING_TABLE& parsing_table)
    : grammar(grammar), parsing_table(parsing_table) {

    ResetStack();
    instruction_pointer = 0;
}


void PDA_Parser::ParseDry(const std::vector<LSyntax::Token>& input_) {
    SetProgram(input_);
    ResetStack();
    while (MoveNext())
    {
        if (LastParserAction() == PDA_ParserAction::PDA_ParserAction_Token) {
            std::cout << "Reducing Terminal: " << CurrentToken() << std::endl;
        } else if (LastParserAction() == PDA_ParserAction_Rule) {
            std::cout << "Applying rule: " << CurrentRule() << std::endl;
        }
    }
    bool success = IsValid();
    if (success) {
        std::cout << "Parser result = Success" << std::endl;
    } else {
        std::cout << "Parser result = Failure" << std::endl;
    }
}
void PDA_Parser::SetProgram(const std::vector<LSyntax::Token>& input_) {
    this->input = input_;
    instruction_pointer = 0;
}
void PDA_Parser::ResetStack() {
    stack.clear();
    stack.push_back(LSyntax::EndOfStr);
    stack.push_back(grammar.axiom);
}
bool PDA_Parser::MoveNext() {
    if (stack.size() == 0) {
        std::cout << "PDA_Parser::MoveNext::ERROR:: Stack is empty" << std::endl;
        return false;
    }
    const LSyntax::Token& stack_top = stack[stack.size() - 1]; 
    if (stack_top == LSyntax::EndOfStr) {
        lastParserAction = PDA_ParserAction::PDA_ParserAction_Token;
        currentToken = stack_top;
        return false;
    }

    const LSyntax::Token& input_token = input[instruction_pointer];
    if (stack_top == input_token) {
        lastParserAction = PDA_ParserAction::PDA_ParserAction_Token;
        currentToken = input_token;
        instruction_pointer++;
        stack.pop_back();
        return true;
    } else if (stack_top.type == LSyntax::TokenType::TERMINAL) {
        std::cout << "Error: stack top not equal input str" << std::endl;
        return false;
    } else if (parsing_table.find(stack_top) == parsing_table.end() ||
                (parsing_table.at(stack_top).find(input_token) == parsing_table.at(stack_top).end() &&
                 parsing_table.at(stack_top).find(LSyntax::Lambda) == parsing_table.at(stack_top).end())) {
        std::cout << "Error: wont be able to derive this string" << std::endl;
        return false;
    }

    lastParserAction = PDA_ParserAction::PDA_ParserAction_Rule;
    bool lambda_rule = false;
    if (parsing_table.at(stack_top).find(input_token) == parsing_table.at(stack_top).end()) {
        lambda_rule = true;
    }
    currentRule = lambda_rule ? parsing_table.at(stack_top).at(LSyntax::Lambda) : parsing_table.at(stack_top).at(input_token);
    stack.pop_back();
    int rule_len = currentRule.value.size();
    for (int i = rule_len - 1; i >= 0; --i) {
        if (currentRule.value[i] != LSyntax::Lambda) {
            stack.push_back(currentRule.value[i]);
        }
    }

    return true;
}
PDA_ParserAction PDA_Parser::LastParserAction() {
    return lastParserAction;
}
LSyntax::Token PDA_Parser::CurrentToken() {
    return currentToken;
}
LSyntax::Rule PDA_Parser::CurrentRule() {
    return currentRule;
}

bool PDA_Parser::IsValid() {
    if (stack.size() == 1 && stack[0] == LSyntax::EndOfStr && 
            instruction_pointer == input.size() - 1 && 
            input[instruction_pointer] == LSyntax::EndOfStr) {
        return true;
    } else {
        PrintTokens(input, instruction_pointer);
        PrintTokens(stack);
        std::cout << "result: FAILURE" << std::endl;
        return false;
    }
}