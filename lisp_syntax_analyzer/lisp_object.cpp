#include "lisp_object.h"
#include "lisp_statement.h"

#include <sstream>
#include <string_view>
#include <memory>
#include <iostream>


using namespace std;



namespace LRuntime {


void Bool::Print(std::ostream& os) const {
    // TODO: Implement
    
}

LFunction::LFunction(
    std::string name,
    std::vector<std::string> argument_names,
    std::vector<std::unique_ptr<LAst::LStatement>> body
) : name(name), argument_names(std::move(argument_names)), body(std::move(body)) {}

void LFunction::Print(std::ostream& os) const {
    // TODO: Implement

}





ObjectHolder LFunction::Call(const std::vector<ObjectHolder>& actual_args, Closure& closure) {
    // TODO: Implement
    return ObjectHolder::None();
}

LList::LList(
    std::list<ObjectHolder> args
) : args(std::move(args)) {}

void LList::Print(std::ostream& os) const {
    // TODO: Implement

}

ObjectHolder LList::Get(int i) {
    // TODO: Implement
    return ObjectHolder::None();
}


}