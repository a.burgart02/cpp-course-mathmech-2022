#pragma once

#include <memory>
#include <unordered_map>

#include "lisp_object.h"

namespace LRuntime {

class ObjectHolder {
public:
  ObjectHolder() = default;

  template <typename T>
  static ObjectHolder Own(T&& object) {
    return ObjectHolder(
      std::make_shared<T>(std::forward<T>(object))
    );
  }

  static ObjectHolder Share(Object& object);
  static ObjectHolder None();

  Object& operator*();
  const Object& operator*() const;
  Object* operator->();
  const Object* operator->() const;

  Object* Get();
  const Object* Get() const;

  template <typename T>
  T* TryAs() {
    return dynamic_cast<T*>(this->Get());
  }

  template <typename T>
  const T* TryAs() const {
    return dynamic_cast<const T*>(this->Get());
  }

  explicit operator bool() const;

  bool Immediately() const {
    return immediately;
  }

  ObjectHolder& MakeImmediately() {
    immediately = true;
    return *this;
  }

private:
  ObjectHolder(std::shared_ptr<Object> data) : data(std::move(data)) {
  }

  std::shared_ptr<Object> data;
  bool immediately = false;
};

using Closure = std::unordered_map<std::string, ObjectHolder>;

bool IsTrue(ObjectHolder object);

bool EqualComparator(ObjectHolder lhs, ObjectHolder rhs);

bool LessComparator(ObjectHolder lhs, ObjectHolder rhs);

inline bool NotEqualComparator(ObjectHolder lhs, ObjectHolder rhs) {
  return !EqualComparator(lhs, rhs);
}

inline bool GreaterComparator(ObjectHolder lhs, ObjectHolder rhs) {
  return !LessComparator(lhs, rhs) && !EqualComparator(lhs, rhs);
}

inline bool LessOrEqualComparator(ObjectHolder lhs, ObjectHolder rhs) {
  return !GreaterComparator(lhs, rhs);
}

inline bool GreaterOrEqualComparator(ObjectHolder lhs, ObjectHolder rhs) {
  return !LessComparator(lhs, rhs);
}

}


using ObjectHolder = LRuntime::ObjectHolder;