#pragma once

#include "lisp_object.h"
#include "lisp_object_holder.h"


#include <unordered_map>
#include <string>
#include <functional>
#include <memory>
#include <vector>


namespace LAst {

struct LStatement {
    virtual ~LStatement() = default;
    virtual ObjectHolder Execute(LRuntime::Closure& closure) = 0;
};

template <typename T>
struct LValueStatement : LStatement {
  T value;

  explicit LValueStatement(T v) : value(std::move(v)) {
  }

  ObjectHolder Execute(LRuntime::Closure&) override {
    return ObjectHolder::Share(value);
  }
};

using NumericConst = LValueStatement<LRuntime::Number>;
using StringConst = LValueStatement<LRuntime::String>;
using BoolConst = LValueStatement<LRuntime::Bool>;


struct VarValue : LStatement {
  std::string name;

  explicit VarValue(std::string name);
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};


class Return : public LStatement {
public:
  explicit Return(std::unique_ptr<LStatement> statement)
    : statement(std::move(statement))
  {
  }

  ObjectHolder Execute(LRuntime::Closure& closure) override;

private:
  std::unique_ptr<LStatement> statement;
};


class IfStatement : public LStatement {
public:
  IfStatement(
    std::unique_ptr<LStatement> condition,
    std::unique_ptr<LStatement> if_body,
    std::unique_ptr<LStatement> else_body
  );

  ObjectHolder Execute(LRuntime::Closure& closure) override;

private:
  std::unique_ptr<LStatement> condition, if_body, else_body;
};

class Print : public LStatement {
public:
  explicit Print(std::unique_ptr<LStatement> argument_);

  ObjectHolder Execute(LRuntime::Closure& closure) override;

  static void SetOutputStream(std::ostream& output_stream);

private:
  std::unique_ptr<LStatement> argument;
  static std::ostream* output;
};

struct DefunStatement : LStatement {
  std::string name;
  std::vector<std::string> argument_names;
  std::vector<std::unique_ptr<LStatement>> body;

  DefunStatement(
    std::string name,
    std::vector<std::string> argument_names,
    std::vector<std::unique_ptr<LStatement>> body
  );

  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

struct FuncCallStatement : LStatement {
  std::string name;
  std::vector<std::unique_ptr<LStatement>> args;

  FuncCallStatement(
    std::string name,
    std::vector<std::unique_ptr<LStatement>> args
  );

  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

struct DefvarStatement : LStatement {
  std::string name;
  std::unique_ptr<LStatement> body;

  DefvarStatement(
    std::string name,
    std::unique_ptr<LStatement> body
  );

  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

struct ListStatement : LStatement {
  std::vector<std::unique_ptr<LStatement>> args;

  ListStatement(
    std::vector<std::unique_ptr<LStatement>> args
  );

  ObjectHolder Execute(LRuntime::Closure& closure) override;
};


class BinaryOperation : public LStatement {
public:
  BinaryOperation(std::unique_ptr<LStatement> lhs, std::unique_ptr<LStatement> rhs)
    : lhs(std::move(lhs))
    , rhs(std::move(rhs))
  {
  }

protected:
  std::unique_ptr<LStatement> lhs, rhs;
};

class UnaryOperation : public LStatement {
public:
  UnaryOperation(std::unique_ptr<LStatement> argument) : argument(std::move(argument)) {
  }

protected:
  std::unique_ptr<LStatement> argument;
};


class Add : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Sub : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Mult : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Div : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Or : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class And : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Not : public UnaryOperation {
public:
  using UnaryOperation::UnaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Comparison : public LStatement {
public:
  using Comparator = std::function<bool(const ObjectHolder&, const ObjectHolder&)>;

  Comparison(
    Comparator cmp,
    std::unique_ptr<LStatement> lhs,
    std::unique_ptr<LStatement> rhs
  );

  ObjectHolder Execute(LRuntime::Closure& closure) override;

private:
  Comparator cmp;
  std::unique_ptr<LStatement> lhs, rhs;
};





class Nth : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Cons : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Rest : public UnaryOperation {
public:
  using UnaryOperation::UnaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Sort : public UnaryOperation {
public:
  using UnaryOperation::UnaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class Car : public UnaryOperation {
public:
  using UnaryOperation::UnaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class MapList : public BinaryOperation {
public:
  using BinaryOperation::BinaryOperation;
  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

class QuoteStatement : public LStatement {
public:
  std::string name;
  std::unique_ptr<LStatement> body;

  explicit QuoteStatement(std::string name);
  explicit QuoteStatement(std::unique_ptr<LStatement> body);

  ObjectHolder Execute(LRuntime::Closure& closure) override;
};

}