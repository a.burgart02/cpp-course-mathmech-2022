#include <catch2/catch_all.hpp>


#include <sstream>
#include <vector>

#include "lisp_lexer.h"



TEST_CASE("Public: test (print (+ 2 2))", "") {
    LLexer::Tokenizer tokenizer;
    std::istringstream iss("(print (+ 2 2))");
    std::vector<LLexer::LexToken> actual;
    for(int i = 0; i < 100; i++) {
        LLexer::LexToken token = tokenizer.ReadNext(iss);
        actual.push_back(token);
        if (token.type == LLexer::LexTokenType::LEX_END) {
            break;
        }
    }

    std::vector<LLexer::LexToken> expected = {
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_PRINT, "print"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "+"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "2"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "2"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_END, ""),
    };

    REQUIRE(actual == expected);
}

TEST_CASE("Public: test (defvar l1 (cons 8 (list 512 128 0 1024)))", "") {
    LLexer::Tokenizer tokenizer;
    std::istringstream iss("(defvar l1 (cons 8 (list 512 128 0 1024)))");
    std::vector<LLexer::LexToken> actual;
    for(int i = 0; i < 100; i++) {
        LLexer::LexToken token = tokenizer.ReadNext(iss);
        
        actual.push_back(token);
        if (token.type == LLexer::LexTokenType::LEX_END) {
            break;
        }
    }

    std::vector<LLexer::LexToken> expected = {
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_DEFVAR, "defvar"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "l1"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "cons"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "8"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_LIST, "list"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "512"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "128"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "0"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "1024"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_END, ""),
    };

    REQUIRE(actual == expected);
}

TEST_CASE("Public: test (print \"some text\")", "") {
    LLexer::Tokenizer tokenizer;
    std::istringstream iss("(print \"some text\")");
    std::vector<LLexer::LexToken> actual;
    for(int i = 0; i < 100; i++) {
        LLexer::LexToken token = tokenizer.ReadNext(iss);
        actual.push_back(token);
        if (token.type == LLexer::LexTokenType::LEX_END) {
            break;
        }
    }

    std::vector<LLexer::LexToken> expected = {
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_PRINT, "print"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_LITERAL, "some text"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_END, ""),
    };

    REQUIRE(actual == expected);
}


TEST_CASE("Public: test (print \"some text)", "") {
    LLexer::Tokenizer tokenizer;
    std::istringstream iss("(print \"some text)");
    std::vector<LLexer::LexToken> actual;
    for(int i = 0; i < 100; i++) {
        LLexer::LexToken token = tokenizer.ReadNext(iss);
        actual.push_back(token);
        if (token.type == LLexer::LexTokenType::LEX_END) {
            break;
        }
    }

    std::vector<LLexer::LexToken> expected = {
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_PRINT, "print"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_LITERAL, "some text)"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_END, ""),
    };

    REQUIRE(actual == expected);
}


TEST_CASE("Public: test defun with multiple lines", "") {
    LLexer::Tokenizer tokenizer;
    std::stringstream iss;
    iss << "(defun double (a)\n";
    iss << "   (return (+ a a))\n";
    iss << ")\n";
    std::vector<LLexer::LexToken> actual;
    for(int i = 0; i < 100; i++) {
        LLexer::LexToken token = tokenizer.ReadNext(iss);
        actual.push_back(token);
        if (token.type == LLexer::LexTokenType::LEX_END) {
            break;
        }
    }
    // ( DEFUN ID ( ID ) ( RETURN ( ID ID ID ) ) ) 
    std::vector<LLexer::LexToken> expected = {
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_DEFUN, "defun"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "double"),

        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "a"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),

        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_RETURN, "return"),
    
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "+"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "a"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "a"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),

        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),

        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_END, ""),
    };

    REQUIRE(actual == expected);
}

TEST_CASE("Public: test multiple statements", "") {
    // (defvar l1 (cons 8 (list 512 128 0 1024)))
    // (defvar l2 (sort l1))
    // (print (cdr l1))
    // (print (car l1))
    // (print (nth 3 l2))
    LLexer::Tokenizer tokenizer;
    std::stringstream iss;
    iss << "(defvar l1 (cons 8 (list 512 128 0 1024)))\n";
    iss << "(defvar l2 (sort l1))\n";
    iss << "(print (cdr l1))\n";
    iss << "(print (car l1))\n";
    iss << "(print (nth 3 l2))\n";
    std::vector<LLexer::LexToken> actual;
    for(int i = 0; i < 100; i++) {
        LLexer::LexToken token = tokenizer.ReadNext(iss);
        actual.push_back(token);
        if (token.type == LLexer::LexTokenType::LEX_END) {
            break;
        }
    }
    // ( DEFVAR ID ( ID NUMBER ( LIST NUMBER NUMBER NUMBER NUMBER ) ) )
    // ( DEFVAR ID ( ID ID ) )
    // ( PRINT ( ID ID ) )
    // ( PRINT ( ID ID ) )
    // ( PRINT ( ID NUMBER ID ) )
    std::vector<LLexer::LexToken> expected = {
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_DEFVAR, "defvar"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "l1"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "cons"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "8"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_LIST, "list"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "512"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "128"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "0"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "1024"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),

        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_DEFVAR, "defvar"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "l2"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "sort"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "l1"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),

        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_PRINT, "print"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "cdr"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "l1"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),

        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_PRINT, "print"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "car"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "l1"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),

        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_PRINT, "print"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_OP, "("),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "nth"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_NUMBER, "3"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_ID, "l2"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        LLexer::LexToken(LLexer::LexTokenType::LEX_CP, ")"),
        
        LLexer::LexToken(LLexer::LexTokenType::LEX_END, ""),
    };

    REQUIRE(actual == expected);
}
