#include "lisp_lexer.h"
#include <memory>

namespace LLexer {


LexToken::LexToken(LexTokenType type, std::string lexem)
    : type(type), lexem(std::move(lexem)) {}


bool operator==(const LexToken& lhs, const LexToken& rhs) {
    return std::tie(lhs.type, lhs.lexem) == std::tie(rhs.type, rhs.lexem);
}


LexToken Tokenizer::ReadNext(std::istream& in) {
    // TODO: Implement
    return LexToken(LexTokenType::LEX_END, "");
}



std::string Tokenizer::ReadId(std::istream& in) {
    // TODO: Implement
    return "";
}


std::string Tokenizer::ReadNumber(std::istream& in) {
    // TODO: Implement
    return "";
}


std::string Tokenizer::ReadLiteral(std::istream& in) {
    // TODO: Implement
    return "";
}


void Tokenizer::SkipSpaces(std::istream& in) {
    // TODO: Implement
}


}