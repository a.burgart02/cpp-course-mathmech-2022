В этой тренировочной задаче вам надо поддержать загрузку расходов из формата XML в нашей программе управления личными финансами. При этом вам надо воспользоваться готовой библиотекой работы с XML. Более конкретно, вам надо написать функцию `vector<Spending> LoadFromXml(istream& input)`. Spending — это структура вида:  

```c++
struct Spending {
  string category;
  int amount;
};
```

Поток input содержит описание расходов в формате XML, например:  

```xml
<july>
  <spend amount="2500" category="food"></spend>
  <spend amount="1150" category="transport"></spend>
  <spend amount="5780" category="restaurants"></spend>
  <spend amount="7500" category="clothes"></spend>
  <spend amount="23740" category="travel"></spend>
  <spend amount="12000" category="sport"></spend>
</july>
```

Важно отметить:

- формат текста в потоке input всегда строго такой, как в примере: есть один корневой узел, который содержит несколько узлов <spend> с атрибутами "amount" и "category"

- функция Load из выданной вам библиотеки работы с XML умеет загружать описанный выше формат

- пример использования библиотеки возможно посмотреть в файле `test_xml_lib.cpp`

Реализуйте функцию `LoadFromXml` в файле `spendings.h`.
