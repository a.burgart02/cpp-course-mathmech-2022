#pragma once

#include "xml.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

struct Spending {
  std::string category;
  int amount;
};

bool operator == (const Spending& lhs, const Spending& rhs) {
  return lhs.category == rhs.category && lhs.amount == rhs.amount;
}

std::ostream& operator << (std::ostream& os, const Spending& s) {
  return os << '(' << s.category << ": " << s.amount << ')';
}

std::vector<Spending> LoadFromXml(std::istream& input) {
  // Реализуйте эту функцию с помощью библиотеки xml.h
  
}