#include <catch2/catch_all.hpp>


#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

using namespace Xml;


TEST_CASE("libXml::Public::TestXmlLib", "") {
  // Пример использования библиотеки xml.h

  std::istringstream xml_input(R"(<johan>
    <spend amount="2500" category="apples"></spend>
    <spend amount="12345" category="tickets"></spend>
    <spend amount="1100" category="flowers"></spend>
  </johan>)");

  Document doc = Load(xml_input);
  const Node& root = doc.GetRoot();
  const std::string actualName = std::string(root.Name());
  const size_t actualChildrenCount = root.Children().size();

  REQUIRE(actualName == "johan");
  REQUIRE(actualChildrenCount == 3u);

  const Node& apples = root.Children().front();
  const std::string actualAppleCategory = std::string(apples.AttributeValue<std::string>("category"));
  const int actualAppleAmount = apples.AttributeValue<int>("amount");
  REQUIRE(actualAppleCategory == "apples");
  REQUIRE(actualAppleAmount == 2500);

  const Node& flowers = root.Children().back();
  const std::string actualFlowersCategory = std::string(flowers.AttributeValue<std::string>("category"));
  const int actualFlowersAmount = flowers.AttributeValue<int>("amount");
  REQUIRE(actualFlowersCategory == "flowers");
  REQUIRE(actualFlowersAmount == 1100);

  Node johan("johan", {});
  Node books("spend", {{"category", "books"}, {"amount", "1150"}});
  johan.AddChild(books);
  const size_t actualJohanChildrenSize = johan.Children().size();
  REQUIRE(actualJohanChildrenSize == 1u);
}

