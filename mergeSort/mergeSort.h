#pragma once

#include <algorithm>


template <typename RandomIt>
void MergeSort2(RandomIt range_begin, RandomIt range_end);

template <typename RandomIt>
void MergeSort3(RandomIt range_begin, RandomIt range_end);
