cmake_minimum_required(VERSION 3.10)

project(cpp_test_system LANGUAGES CXX VERSION 1.0)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED YES)
set(CMAKE_CXX_EXTENSIONS NO)
# FIXME: clashes with folder name on Linux
# can find executables in out/build/<name>/<name>(.exe)
# set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)


# find_package if needed
find_package(Catch2 3 REQUIRED)

#include add_hook for subdirs
include(cmake/utils.cmake)

option(TASK_NAME "" "")

if (TASK_NAME STREQUAL "OFF")
    if (EXISTS "add/CMakeLists.txt")
        add_subdirectory(add)
    endif()

    if (EXISTS "sub/CMakeLists.txt")
        add_subdirectory(sub)
    endif()

    if (EXISTS "lisp_lexical_analyzer/CMakeLists.txt")
        add_subdirectory(lisp_lexical_analyzer)
    endif()

    if (EXISTS "lisp_syntax_analyzer/CMakeLists.txt")
        add_subdirectory(lisp_syntax_analyzer)
    endif()

    if (EXISTS "deque/CMakeLists.txt")
        add_subdirectory(deque)
    endif()

    if (EXISTS "josephusPermutation/CMakeLists.txt")
        add_subdirectory(josephusPermutation)
    endif()

    if (EXISTS "linkedList/CMakeLists.txt")
        add_subdirectory(linkedList)
    endif()

    if (EXISTS "mergeSort/CMakeLists.txt")
        add_subdirectory(mergeSort)
    endif()

    if (EXISTS "permutations/CMakeLists.txt")
        add_subdirectory(permutations)
    endif()

    if (EXISTS "secondaryIndex/CMakeLists.txt")
        add_subdirectory(secondaryIndex)
    endif()

    if (EXISTS "hashSet/CMakeLists.txt")
        add_subdirectory(hashSet)
    endif()

    if (EXISTS "libXml/CMakeLists.txt")
        add_subdirectory(libXml)
    endif()

    if (EXISTS "libJson/CMakeLists.txt")
        add_subdirectory(libJson)
    endif()

    if (EXISTS "libIni/CMakeLists.txt")
        add_subdirectory(libIni)
    endif()

else()
    if (EXISTS "${TASK_NAME}/CMakeLists.txt")
        add_subdirectory("${TASK_NAME}")
    endif()
endif()
