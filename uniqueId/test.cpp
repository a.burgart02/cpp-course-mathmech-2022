#include <catch2/catch_all.hpp>


#include "uniqueId.h"
#include <iostream>
#include <string>
#include <vector>


TEST_CASE("UNIQUE_ID::Public::UniquePerLine", "") { 
  int UNIQ_ID = 0;
  std::string UNIQ_ID = "hello";
  std::vector<std::string> UNIQ_ID = {"hello", "world"};
  std::vector<int> UNIQ_ID = {1, 2, 3, 4};
  
  REQUIRE(true);
}

TEST_CASE("UNIQUE_ID::Public::worksInIfElseWhileFor", "") {
  if (2 * 2 == 4)
    int UNIQ_ID = 1;
  else
    int UNIQ_ID = 0;

  for (int UNIQ_ID = 0; UNIQ_ID < 5; UNIQ_ID++)
    std::string UNIQ_ID = "hello";
  
  while(bool UNIQ_ID = false)
    std::vector<std::string> UNIQ_ID = {"hello", "world"};

  REQUIRE(true);
}

#define TO_STRING_1(name) std::string(#name) 
#define TO_STRING(name) TO_STRING_1(name) 

TEST_CASE("UNIQUE_ID::Public::VarNameContainsLineNumber", "") {
  std::string varName = TO_STRING(UNIQ_ID); std::string lineStr = std::to_string(__LINE__);
  bool containsLineNumber = varName.find(lineStr) != std::string::npos;
  
  REQUIRE(containsLineNumber);
}
