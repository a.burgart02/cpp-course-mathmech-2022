# Задача Макрос UNIQ_ID

Разработать макрос `UNIQ_ID`, который будет формировать идентификатор, уникальный в пределах данного файла. Например, следующий код должен компилироваться и работать:

```c++
int UNIQ_ID = 5;
string UNIQ_ID = "hello!";
```

В рамках данной задачи допускается, что код не будет компилироваться:

```c++
int UNIQ_ID = 5; string UNIQ_ID = "hello"; // оба определения на одной строке
```

Возможно вам будет полезно прочитать документацию по [директивам](https://learn.microsoft.com/ru-ru/cpp/preprocessor/preprocessor-directives?view=msvc-170) и [операторам](https://learn.microsoft.com/ru-ru/cpp/preprocessor/preprocessor-operators?view=msvc-170) препроцессора