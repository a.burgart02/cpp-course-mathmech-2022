
#pragma onces
#include <vector>

class Permutations
{
public:
    Permutations(int n);

    bool next();

    const std::vector<int>& current();

private:
    std::vector<int> _current;
};

