#include <catch2/catch_all.hpp>


#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

using namespace Json;


TEST_CASE("libJson::Public::TestJsonLib", "") {
  // Пример использования библиотеки json.h

  std::istringstream json_input(R"([
    {"amount": 2500, "category": "apples"},
    {"amount": 1150, "category": "books"},
    {"amount": 1100, "category": "flowers"}
  ])");

  Document doc = Load(json_input);
  const std::vector<Node>& root = doc.GetRoot().AsArray();
  REQUIRE(root.size() == 3u);

  const std::map<std::string, Node>& apples = root.front().AsMap();
  REQUIRE(apples.at("category").AsString() == "apples");
  REQUIRE(apples.at("amount").AsInt() == 2500);

  const std::map<std::string, Node>& flowers = root.back().AsMap();
  REQUIRE(flowers.at("category").AsString() == "flowers");
  REQUIRE(flowers.at("amount").AsInt() == 1100);

  Node books(std::map<std::string, Node>{{"category", Node("books")}, {"amount", Node(1150)}});
  Node array_node(std::vector<Node>{books});
  REQUIRE(array_node.AsArray().size() == 1u);
}