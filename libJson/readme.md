В этой тренировочной задаче вам надо поддержать загрузку расходов из формата JSON в нашей программе управления личными финансами. При этом вам надо воспользоваться готовой библиотекой работы с JSON. Более конкретно, вам надо написать функцию `vector<Spending> LoadFromJson(istream& input)`. Spending — это структура вида:  

```c++
struct Spending {
  string category;
  int amount;
};
```

  Поток input содержит описание расходов в формате JSON, например:

```json
[
  {"amount": 2500, "category": "food"},
  {"amount": 1150, "category": "transport"},
  {"amount": 5780, "category": "restaurants"},
  {"amount": 7500, "category": "clothes"},
  {"amount": 23740, "category": "travel"},
  {"amount": 12000, "category": "sport"}
]
```


Важно отметить:

- формат текста в потоке input всегда строго такой, как в примере: есть массив, который содержит несколько словарей с ключами "amount" и "category"

- функция Load из выданной вам библиотеки работы с JSON умеет загружать описанный выше формат

- пример использования библиотеки возможно посмотреть в файле `test_json_lib.cpp`

Реализуйте функцию `LoadFromJson` в файле `spendings.cpp`.
