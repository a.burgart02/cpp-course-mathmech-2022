#include "json.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>


struct Spending {
  std::string category;
  int amount;
};

bool operator == (const Spending& lhs, const Spending& rhs);

std::ostream& operator << (std::ostream& os, const Spending& s);

int CalculateTotalSpendings(
  const std::vector<Spending>& spendings
);

std::string MostExpensiveCategory(
  const std::vector<Spending>& spendings
);

std::vector<Spending> LoadFromJson(std::istream& input);