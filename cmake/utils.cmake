option(TEST_SOLUTION "Build solution (include to project)" ON)
option(ENABLE_PRIVATE_TEST "Build solution (include to project)" ON)

set(PRIVATE_COURSE_LOCATION "private-cpp-course-mathmech")

function(course_configure_include_directories TARGET)
    if (TEST_SOLUTION)
        get_filename_component(TASK_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
        target_include_directories(${TARGET}
            PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/../${PRIVATE_COURSE_LOCATION}/${TASK_NAME}")
        message(STATUS "INFO: including ${CMAKE_CURRENT_SOURCE_DIR}/../${PRIVATE_COURSE_LOCATION}/${TASK_NAME}")
    endif()

    target_include_directories(${TARGET}
        PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
    message(STATUS "INFO: including ${CMAKE_CURRENT_SOURCE_DIR}")
endfunction()


function(course_prepend VAR PREFIX)
    set(LIST_VAR "")
    foreach(ELEM ${ARGN})
        list(APPEND LIST_VAR "${PREFIX}/${ELEM}")
    endforeach()
    set(${VAR} "${LIST_VAR}" PARENT_SCOPE) # like ref
endfunction()


function(hook_executable TARGET)
    set(MULTI_VALUE_ARGS SLN_SRCS PRIVATE_TESTS)
    # prefix, bool_vars, mono_val_vars, mono_var_names, multi_var_names
    cmake_parse_arguments(COURSE_LIB "" "" "${MULTI_VALUE_ARGS}" ${ARGN})
    
    get_filename_component(TASK_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    if (TEST_SOLUTION)
        message(STATUS "TEST_SOLUTION is ON")
        course_prepend(COURSE_LIB_SLN_SRCS "${CMAKE_CURRENT_SOURCE_DIR}/../${PRIVATE_COURSE_LOCATION}/${TASK_NAME}" ${COURSE_LIB_SLN_SRCS})
    else()
        unset(COURSE_LIB_SLN_SRCS)
    endif()
    
    if (ENABLE_PRIVATE_TEST STREQUAL ON)
        message(STATUS "ENABLE_PRIVATE_TEST is ON")
        list(APPEND COURSE_LIB_PRIVATE "test.cpp")
        # filter files if needed
        # list(FILTER COURSE_LIB_UNPARSED_ARGUMENTS EXCLUDE REGEX "pattern")
    else()
        message(STATUS "ENABLE_PRIVATE_TEST is OFF")
        # clear if needed
        # unset(COURSE_LIB_PRIVATE)
    endif()
    course_prepend(COURSE_LIB_PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/../${PRIVATE_COURSE_LOCATION}/${TASK_NAME}" ${COURSE_LIB_PRIVATE})
    message("INFO: PRIVATE_TESTS prepended: ${COURSE_LIB_PRIVATE}")

    add_executable(${TASK_NAME}
        ${COURSE_LIB_UNPARSED_ARGUMENTS}
        ${COURSE_LIB_SLN_SRCS}
        ${COURSE_LIB_PRIVATE})
    
    if (ENABLE_PRIVATE_TEST STREQUAL ON)
        target_include_directories(${TASK_NAME} PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/../${PRIVATE_COURSE_LOCATION}/${TASK_NAME}")
    endif()

    message(STATUS "TASK_NAME = ${TASK_NAME}
    COURSE_LIB_UNPARSED_ARGUMENTS = ${COURSE_LIB_UNPARSED_ARGUMENTS}
    COURSE_LIB_SLN_SRCS = ${COURSE_LIB_SLN_SRCS}
    COURSE_LIB_PRIVATE = ${COURSE_LIB_PRIVATE}")
    
    course_configure_include_directories(${TASK_NAME})

    target_link_libraries(${TASK_NAME} PRIVATE Catch2::Catch2WithMain)
endfunction()


# FUNCTION TO CONFIGURE TASK
function(add_hook TARGET)
    message(STATUS "add_hook: TARGET = ${TARGET} ARGS = ${ARGN}")

    hook_executable(${TARGET} ${ARGN})
endfunction()


function(print_include_dirs)
    get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
    message(STATUS "INCLUDE DIRECTORIES: ")
    foreach(dir ${dirs})
        message(STATUS "dir='${dir}'")
    endforeach()
endfunction()
