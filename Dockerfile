FROM gcc:10.3-buster

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100

RUN apt-get update \
  	&& apt-get install -y \
	make \
	cmake \
	git \
	wget \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

RUN apt-get update \
	&& apt-get install -y \
	build-essential \
	zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev \
	libssl-dev libreadline-dev libffi-dev libsqlite3-dev libbz2-dev \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir /tmp/catch2-src \
    && cd /tmp/catch2-src \ 
	&& git clone https://github.com/catchorg/Catch2.git \
	&& cd Catch2 \
	&& cmake -Bbuild -H. -DBUILD_TESTING=OFF \
	&& cmake --build build/ --target install \
	&& cd ~ \
	&& rm -rf /tmp/catch2-src

RUN apt-get update \
  	&& apt-get install -y \
	clang \
	llvm \
	clang-tidy \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir /tmp/python-src \
	&& cd /tmp/python-src \ 
	&& wget https://www.python.org/ftp/python/3.10.5/Python-3.10.5.tgz \
	&& tar -xf Python-3.10.*.tgz \
	&& cd Python-3.10.*/ \
	&& ./configure --enable-optimizations \
    && make -j$(nproc) \
    && make altinstall \
	&& cd ~ \
	&& rm -rf /tmp/python-src

WORKDIR /opt/cpp_course
COPY . .

RUN cd grader && \
	python3.10 -m venv .venv && \
	./.venv/bin/python3.10 -m pip install --upgrade pip && \
	./.venv/bin/python3.10 -m pip install poetry && \
	./.venv/bin/poetry install
