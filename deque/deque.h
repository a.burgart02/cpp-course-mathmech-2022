#pragma once

#include <vector>


template <typename T>
class Deque {
public:
    T const& operator[](int index) const;
    T& operator[](int index);
    void PushFront(const T& val);
    void PushBack(const T& val);
    bool Empty() const;
    size_t Size() const;
    const T& At(size_t index) const;
    T& At(size_t index);
    const T& Front() const;
    T& Front();
    const T& Back() const;
    T& Back();
private:
    std::vector<T> front_data;
    std::vector<T> back_data;
};
